// Company name : PCSPL
// Project : Encryption / Decryption
// Descryption : Encrypt and Decrypt string  
// Date : 15/01/2018
// Version : 1.0.0
// Author : RR
// repository
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;
using System.Threading;




namespace crypto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
        }

        //Timer t1 = new Timer();
        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Start");
            textBox1.Text = "Enc";
            try
            {
                // Create a new TripleDESCryptoServiceProvider object
                // to generate a key and initialization vector (IV).
                TripleDESCryptoServiceProvider tDESalg = new TripleDESCryptoServiceProvider();

                // Create a string to encrypt.
                string sData = "Password@1234.";
                //string FileName = "CText.txt";

                // Encrypt text to a file using the file name, key, and IV.
                byte[] Data = EncryptTextToMemory(sData, tDESalg.Key, tDESalg.IV);
                string str = Encoding.ASCII.GetString(Data, 0, Data.Length);
                // Decrypt the text from a file using the file name, key, and IV.
                //string Final = DecryptTextFromFile(FileName, tDESalg.Key, tDESalg.IV);

                // Display the decrypted string to the console.
                //Console.WriteLine(Final);
                
                textBox1.Text = str + "data end ";
                Debug.WriteLine("Encrypt output:----" + str );

                string Final = DecryptTextFromMemory(Data, tDESalg.Key, tDESalg.IV);
                Debug.WriteLine("Decrypt output:----" + Final);

             }
            catch (Exception e2)
            {
                MessageBox.Show("e" + e2.Message);
            }
       

        }

        public static byte[] EncryptTextToMemory(string Data, byte[] Key, byte[] IV)
        {

            try
            {

                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream,
                new TripleDESCryptoServiceProvider().CreateEncryptor(Key, IV),
                CryptoStreamMode.Write);

                // Convert the passed string to a byte array.
                byte[] toEncrypt = new ASCIIEncoding().GetBytes(Data);

                // Write the byte array to the crypto stream and flush it.
                cStream.Write(toEncrypt, 0, toEncrypt.Length);
                cStream.FlushFinalBlock();
                 
                // Get an array of bytes from the 
                // MemoryStream that holds the 
                // encrypted data.
                byte[] ret = mStream.ToArray();
                //byte[] ret =  mStream.ToString();
                //byte[] ret = System.Text.ASCIIEncoding.Default.GetBytes(mStream);
                // Close the streams.
          
               
                cStream.Close();
                mStream.Close();
                
                // Return the encrypted buffer.
                return ret;
                

            }


            catch (CryptographicException e)
            {
                MessageBox.Show("A Cryptographic error occurred: {0}" +  e.Message);
                return null;

            }
            catch (UnauthorizedAccessException e)
            {
                MessageBox.Show("A file access error occurred: {0}" + e.Message);
                return null;
            }

        }

        public static string DecryptTextFromMemory(byte[] Data, byte[] Key, byte[] IV)
        {
            try
            {
                // Create a new MemoryStream using the passed 
                // array of encrypted data.
                MemoryStream msDecrypt = new MemoryStream(Data);

                // Create a CryptoStream using the MemoryStream 
                // and the passed key and initialization vector (IV).
                CryptoStream csDecrypt = new CryptoStream(msDecrypt,
                    new TripleDESCryptoServiceProvider().CreateDecryptor(Key, IV),
                    CryptoStreamMode.Read);

                // Create buffer to hold the decrypted data.
                byte[] fromEncrypt = new byte[Data.Length];

                // Read the decrypted data out of the crypto stream
                // and place it into the temporary buffer.
                csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);

                //Convert the buffer into a string and return it.
                return new ASCIIEncoding().GetString(fromEncrypt, 0, fromEncrypt.Length);
            }
            catch (CryptographicException e)
            {
                MessageBox.Show("A Cryptographic error occurred: {0}", e.Message);
                return null;
            }
        }


        //encrypt 

        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            string key = "mykey";
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        //decrypt

        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);
            string key = "mykey";

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there  are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray, 0, resultArray.Length);
        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            string clearText = textBox1.Text.Trim();
            string cipherText = Encrypt(clearText, true);
            Debug.WriteLine("Encrypt output:----" + cipherText);
            textBox1.Text = cipherText;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            string cipherText = textBox1.Text.Trim();
            string decryptedText = Decrypt(cipherText, true);
            textBox1.Text = decryptedText;
            Debug.WriteLine("Decrypy output:----" + decryptedText);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
         

            double k;
            k=0.01;
            GenTimeSpanFromSeconds(k);

            ////if (t1.Interval < 20000)
            ////{
            ////    //double seconds;
            ////    textBox2.Text = DateTime.Now.ToString();
            ////    textBox1.Text = "Time";
            ////    //TimeSpan interval = TimeSpan.FromSeconds(seconds);
            ////    //string timeInterval = interval.ToString();

            ////    textBox1.Text = "Sec" ;
            ////    //t1.Enabled = false;
            ////}
            ////else
            ////{
            ////    long i;
            ////    //timer1.Interval = Convert.ToInt32(TimeSpan.FromTicks(i)); 
            ////    textBox2.Text = DateTime.Now.ToString();
            ////    t1.Enabled = false;
            ////} 

        }

        private void button5_Click(object sender, EventArgs e)
        {


            DirectoryInfo di = new DirectoryInfo("\\");
            // Change to database file name  
            string path = "\\CText.txt"; 
             
            //1,073,741,824 
            long filesize;
            
            FileInfo fi1 = new FileInfo(path);
            //filesize = (fi1.Length / 1073741824);// GB
            filesize = (fi1.Length / 1048576); // MB 
 
            Debug.WriteLine("The size of {0} is {1} bytes." + fi1.Name + "Size: " + fi1.Length);
            textBox1.Text = Convert.ToString(filesize);// show size
            //// new change
                   
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //Timer t1 = new Timer();
            ////t1.Tick += new EventHandler(timer1_Tick);
            ////t1.Interval = (1000) * (5);              // Timer will tick evert second
            ////t1.Enabled = true;
            // Enable the timer
            //t1.Enabled   
            // Start the timer
            //if (t1.Interval >= 20000)
            //{
            //    textBox2.Text = "Reset";
            //    t1.Enabled = false; 
            //}
            //else
            //{
            //    textBox2.Text = "Start";

            //} 
        }


        static void GenTimeSpanFromSeconds(double seconds)
        {
            // Create a TimeSpan object and TimeSpan string from 
            // a number of seconds.
            TimeSpan interval = TimeSpan.FromSeconds(seconds);
            string timeInterval = interval.ToString();
            TimeSpan.TicksPerMinute();


            // Pad the end of the TimeSpan string with spaces if it 
            // does not contain milliseconds.
            int pIndex = timeInterval.IndexOf(':');
            pIndex = timeInterval.IndexOf('.', pIndex);
            if (pIndex < 0) timeInterval += "        ";

            Debug.WriteLine("{0,21}{1,26}" + seconds + timeInterval );
        } 

    }
}